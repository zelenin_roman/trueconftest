package com.romazelenin.trueconftest

import android.animation.AnimatorSet
import android.animation.TimeInterpolator
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.animation.AccelerateInterpolator
import android.view.animation.LinearInterpolator
import android.widget.FrameLayout
import android.widget.TextView
import androidx.core.animation.doOnEnd
import androidx.core.animation.doOnRepeat
import com.google.android.material.animation.AnimatorSetCompat
import java.util.Locale

class MainActivity : AppCompatActivity() {

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val helloTextView = findViewById<TextView>(R.id.helloTextView)
        val container = (helloTextView.parent as FrameLayout)

        val textColor = getColorForCurrentLocale()

        var animator: AnimatorSet? = null
        helloTextView.setOnClickListener { animator?.cancel() }

        container.setOnTouchListener { _, motionEvent ->
            if (motionEvent.action == MotionEvent.ACTION_DOWN) {
                animator?.cancel()
                helloTextView.setTextColor(textColor)
                helloTextView.x = (motionEvent.x - helloTextView.width / 2).let {
                    if (it < 0) 0f
                    else if (it + helloTextView.width > container.width) {
                        (container.width - helloTextView.width).toFloat()
                    } else it
                }
                helloTextView.y = (motionEvent.y - helloTextView.height / 2).let {
                    if (it < 0) 0f
                    else if (it + helloTextView.height > container.height) {
                        (container.height - helloTextView.height).toFloat()
                    } else it
                }
                animator = AnimatorSet().apply {
                    val commonDuration = 5000L
                    playSequentially(
                        ValueAnimator.ofFloat(
                            helloTextView.y,
                            container.y + container.height - helloTextView.height
                        ).apply {
                            val pxInMs =
                                (container.y + container.height - helloTextView.height) / commonDuration
                            val px = (container.height - helloTextView.y)
                            duration = (px / pxInMs).toLong()
                            addUpdateListener { helloTextView.y = it.animatedValue as Float }
                        },
                        ValueAnimator.ofFloat(
                            container.y + container.height - helloTextView.height,
                            container.y
                        ).apply {
                            repeatMode = ValueAnimator.REVERSE
                            repeatCount = ValueAnimator.INFINITE
                            duration = commonDuration
                            addUpdateListener {
                                helloTextView.y = it.animatedValue as Float
                            }
                        })
                    interpolator = LinearInterpolator()
                    startDelay = 5000
                    start()
                }
            }
            true
        }
    }

    private fun getColorForCurrentLocale(): Int {
        val locale = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            resources.configuration.locales[0]
        } else {
            resources.configuration.locale
        }

        return when (locale.language) {
            "ru" -> Color.BLUE
            "en", "us" -> Color.RED
            else -> Color.WHITE
        }
    }
}